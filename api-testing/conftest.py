"""Default fixtures and classes that are used in all API tests."""

__author__ = 'Jonatas Aimbire'
__copyright__ = 'Forty8Fifty Labs'

import requests
import pytest


class SessionManager:
    """Class that contains all the information for starting a session."""
    def __init__(self):
        """Class Constructor."""
        self._base_url = 'https://jira.qa.forty8fiftylabs.com/{}'
        self._login_url = self._base_url.format('login.jsp')

        self._credentials = {
            'username': 'jonatas.santos@whiteprompt.com',
            'password': 'm%$z$7W7uK23'
        }

        self.session = self._new_session()

    def _new_session(self) -> requests.Session:
        payload = {
            'os_username': self._credentials['username'],
            'os_password': self._credentials['password']
        }

        with requests.Session() as session:
            res = session.post(self._login_url, data=payload)
            assert res.status_code == requests.codes.ok

            return session


@pytest.fixture(scope='session')
def session_manager():
    """Create a ``SessionManager``.

    Returns:
        (:obj:`SessionManager`): The ``SessionManager`` object.

    """
    return SessionManager()


@pytest.fixture(scope='session')
def admin(session_manager):
    """Return the ``admin`` session for ``API`` testing.

    Args:
        session_manager (:obj:`SessionManager`): A ``SessionManager`` object.

    Returns:
        @WIP

    """
    admin = session_manager.session
    return admin
