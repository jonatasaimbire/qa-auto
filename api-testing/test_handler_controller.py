"""WIP."""

__author__ = 'Jonatas Aimbire'
__copyright__ = 'Forty8Fifty Labs'


import requests
from pprint import pprint


def test_get_retrieve_all(admin):
    base_url = 'https://jira.qa.forty8fiftylabs.com/{}'
    url = '/rest/fh/controllers/handler/'

    request_url = base_url.format(url)

    r = admin.get(request_url)
    assert r.status_code == requests.codes.ok
    pprint(r.json())
