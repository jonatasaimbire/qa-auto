from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec


def test_jira_service_desk_connector_overview(wd):
    wait = WebDriverWait(wd, 5)

    open = wait.until(
        ec.visibility_of_element_located(
            (
                By.CSS_SELECTOR,
                '[data-appid="TA-RealTimeJIRAServiceDeskConnectorForSplunk"]'
            )
        )
    )
    open.click()

    element = wd.find_element_by_class_name('dashboard-body')

    assert True
