"""Module that groups all tests regarding the integration of Splunk to Jira."""

__author__ = 'Jonatas Aimbire'
__copyright__ = 'Forty8Fifty Labs'

import pytest
from .helpers.splunk_integration import SplunkIntegration
from selenium.webdriver.support.ui import WebDriverWait


@pytest.mark.splunk_connector
class TestSplunkIntegration:
    """Class with tests at the *RealTime Splunk Connector for JIRA* level."""

    def test_splunk_server_success(self, wd):
        """Verify that the Splunk Scheme is readily available and working.

        Important Configuration:
            ``expected['name']['value']`` must be valid and exist.

        """
        expected = {
            'status': {'i': None, 'value': 'Active'},
            'name': {'i': None, 'value': 'qa_auto'},
            'id': {'i': None, 'value': '18'},
            'details': {'i': None, 'value': 'Success'}
        }
        wait = WebDriverWait(wd, 5)

        wd.find_element_by_id('admin_menu').click()
        wd.find_element_by_id('admin_applications_menu').click()
        wd.find_element_by_id('splunk-link').click()

        table_id = 'splunksTable'
        splunk_schemes = SplunkIntegration.return_table_with_header_and_rows(
            wait, table_id
        )
        expected['status']['i'] = splunk_schemes['header'].index('Status')
        expected['name']['i'] = splunk_schemes['header'].index('Name')
        expected['id']['i'] = splunk_schemes['header'].index('ID')
        expected['details']['i'] = splunk_schemes['header'].index('Details')

        scheme_found = False
        actual = None
        for row in splunk_schemes['rows']:
            row = row.text.split()
            if row[expected['name']['i']] == expected['name']['value']:
                actual = row
                scheme_found = True
                break

        assert scheme_found, 'No Splunk Connector found. Is the name correct?'
        assert actual[expected['status']['i']] == expected['status']['value']
        assert actual[expected['name']['i']] == expected['name']['value']
        assert actual[expected['id']['i']] == expected['id']['value']
        assert actual[expected['details']['i']] == expected['details']['value']
