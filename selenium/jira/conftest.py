"""Default fixtures and classes that are used in all JIRA tests."""

__author__ = 'Jonatas Aimbire'
__copyright__ = 'Forty8Fifty Labs'

import pytest
from selenium import webdriver


class SessionManager:
    """Class that contains all the information for a JIRA session."""
    def __init__(self):
        """Class Constructor."""
        self._base_url = 'https://jira.qa.forty8fiftylabs.com/login.jsp'
        self._credentials = {
            'username': 'jonatas.santos@whiteprompt.com',
            'password': 'm%$z$7W7uK23'
        }

    def driver(self, browser='Chrome'):
        """Return the ``WebDriver`` already logged into JIRA.

        Start the ``WebDriver`` instance for the browser chosen and log in into
        the JIRA software. Afterwards return said ``WebDriver`` for it to be
        used on tests.

        Args:
            browser (str): The browser to run tests. Defaults to 'Chrome'

        Returns:
            (:obj:`WebDriver`): The WebDriver to be used.

        """
        if (browser == 'Chrome'):
            driver_path = 'D:/work/selenium_drivers/chromedriver.exe'
            wd = webdriver.Chrome(driver_path)
        else:
            raise AttributeError('Incorrect web browser defined.')

        self._login_jira(wd)

        return wd

    def _login_jira(self, wd):
        """Log in into JIRA.

        Args:
            wd (:obj:`WebDriver`): The WebDriver to be used.

        """
        wd.get(self._base_url)

        prefix = 'login-form-{}'
        elements = {
            'input': {
                'username': wd.find_element_by_id(prefix.format('username')),
                'password': wd.find_element_by_id(prefix.format('password')),
                'log_in': wd.find_element_by_id(prefix.format('submit'))
            }
        }

        elements['input']['username'].send_keys(self._credentials['username'])
        elements['input']['password'].send_keys(self._credentials['password'])
        elements['input']['log_in'].click()


@pytest.fixture(scope='session')
def session_manager():
    """Create a ``SessionManager``.

    Returns:
        (:obj:`SessionManager`): The ``SessionManager`` object.

    """
    return SessionManager()


@pytest.fixture(scope='session')
def wd(session_manager):
    """Start a ``WebDriver`` for tests.

    Args:
        session_manager (:obj:`SessionManager`): A ``SessionManager`` object.

    Yields:
        driver (:obj:`WebDriver`): The ``WebDriver`` for tests.

    """
    driver = session_manager.driver()
    yield driver
    driver.quit()
