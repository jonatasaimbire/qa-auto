"""Helper Class with methods that are used frequently on
*RealTime Splunk Connector for JIRA*."""

__author__ = 'Jonatas Aimbire'
__copyright__ = 'Forty8Fifty Labs'

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec


class SplunkIntegration:
    """All methods that can be used on *RealTime Splunk Connector for JIRA*."""

    @staticmethod
    def return_table_with_header_and_rows(wait, table_id):
        """Return a full formatted table with the header as a list.

        Args:
            wait (:obj:`WebDriverWait`): The wait with a ``WebDriver``.
            table_id (str): The id from the table element to be formatted.

        Returns:
            element (:obj:`dict`): Formatted table to be used within tests.
        """
        element = {
            'def': wait.until(
                ec.visibility_of_element_located((By.ID, table_id))
            )
        }
        element['rows'] = element['def'].find_elements_by_css_selector(
            'tbody tr'
        )

        for value in element['def'].find_elements_by_css_selector('thead tr'):
            element['header'] = value.text.split('\n')

        return element
